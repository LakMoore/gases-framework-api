# README #

This API exposes the Gases Framework to any mod. It allows you to do whatever Glenn's Gases can do, and probably more. The API will work even when the Gases Framework is not installed, but functionality will be missing for obvious reasons.

To start using the API, just copy this source into your workspace directory. Also make sure to install a dev version of the Gases Framework in the mods directory of your workspace. Note that your mod can still compile and run without installing the Gases Framework dev build.